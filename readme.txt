To build this code you need to have the following prerequisites installed:

1) Pangolin - https://github.com/stevenlovegrove/Pangolin.
2) Eigen3 -  http://eigen.tuxfamily.org
3) OpenCv - Tested with OpenCV 2.4.11 and OpenCV 3.2

To build the code:

cd ORB_SLAM2         
chmod +x build.sh
./build.s 

To run the code:

./Examples/Stereo/stereo_kitti Vocabulary/ORBvoc.txt Examples/Stereo/KITTI00-02.yaml 1


note: the code would not run if a camera is not detected.



Whenever the robot has to go straight, the server would receive Thrust with some value and heading with 0 value. The motors should make the robot go straight until the thrust value becomes 0. the value of the thrust tells the unit
distance that need to be covered in a straight path. Like thrust = 20 means that the robot has to cover 20 units in a straight line.

Whenever the robot has to turn, the server would receive thrust with 0 value. When the server recieves turnState::STARTTURN, this means that the tyres should turn at an angle denoted by the heading's value. The direction of turn would be 
in StepType (StepType::LEFT or StepType::Right). As soons as the server recieves turnState::STOPTURN, this means that the turn has ended. 