

#include <unistd.h>
#include <iostream>
#include <algorithm>
#include <fstream>
#include <iomanip>
#include <chrono>
#include <Tracking.h>

#include <opencv2/core/core.hpp>

#include <System.h>
#include <PathPlanning.h>

#include <grpcpp/grpcpp.h>

#include "robot.grpc.pb.h"

using grpc::Channel;
using grpc::ClientContext;
using grpc::Status;
using robot::HealthCheckRequest;
using robot::Health;
using robot::State;
using robot::Empty;
using robot::Step;
using robot::RobotService;
using robot::StepType;
using robot::Command;
using robot::startStopTurn;
using robot::turnState;
using namespace std;

class RobotControl {
 public:
  RobotControl(std::shared_ptr<Channel> channel)
      : stub_(RobotService::NewStub(channel)) {}




  // Assembles the client's payload, sends it and presents the response back
  // from the server.
  std::string goStraightFor(float straight) {
    // Data we are sending to the server.
    Command request;
    request.set_thrust(straight);

    // Container for the data we expect from the server.
    State reply;

    // Context for the client. It could be used to convey extra information to
    // the server and/or tweak certain RPC behaviors.
    ClientContext context;

    // The actual RPC.
    Status status = stub_->ExecuteCommand(&context, request, &reply);

    // Act upon its status.
    if (status.ok()) {
      return "going sraight";
    } else {
      std::cout << status.error_code() << ": " << status.error_message()
                << std::endl;
      return "RPC failed";
    }
  }

  std::string turnAngle(float Angle) {
    // Data we are sending to the server.
    Command request;
    request.set_heading(Angle);

    // Container for the data we expect from the server.
    State reply;

    // Context for the client. It could be used to convey extra information to
    // the server and/or tweak certain RPC behaviors.
    ClientContext context;

    // The actual RPC.
    Status status = stub_->ExecuteCommand(&context, request, &reply);

    // Act upon its status.
    if (status.ok()) {
      return "turning";
    } else {
      std::cout << status.error_code() << ": " << status.error_message() << std::endl;
      return "RPC failed";
    }
  }

  std::string turnDirection(char Direction) {
    // Data we are sending to the server.
    Step request;

    if (Direction == 'l'){
      request.set_type(StepType::LEFT);
    }


	if (Direction == 'r'){
      request.set_type(StepType::RIGHT);
    }


    // Container for the data we expect from the server.
    State reply;

    // Context for the client. It could be used to convey extra information to
    // the server and/or tweak certain RPC behaviors.
    ClientContext context;

    // The actual RPC.
    Status status = stub_->ExecuteStep(&context, request, &reply);

    // Act upon its status.
    if (status.ok()) {
      return "TurnDirection";
    } else {
      std::cout << status.error_code() << ": " << status.error_message() << std::endl;
      return "RPC failed";
    }
  }


  std::string turnState(int turnState) {
    // Data we are sending to the server.
    startStopTurn request;

	if (turnState == 1){
      request.set_turning(turnState::STARTTURN);
    }


	if (turnState == 0){
      request.set_turning(turnState::STOPTURN);
    }


    // Container for the data we expect from the server.
    State reply;

    // Context for the client. It could be used to convey extra information to
    // the server and/or tweak certain RPC behaviors.
    ClientContext context;

    // The actual RPC.
    Status status = stub_->TurnState(&context, request, &reply);

    // Act upon its status.
    if (status.ok()) {
      return "TurnState";
    } else {
      std::cout << status.error_code() << ": " << status.error_message() << std::endl;
      return "RPC failed";
    }
  }
  
 private:
  std::unique_ptr<RobotService::Stub> stub_;
};


int main(int argc, char **argv) {
 
  char Direction = 'l';

  RobotControl controller(grpc::CreateChannel("localhost:50051", grpc::InsecureChannelCredentials()));
		
  string test1= controller.goStraightFor(1);
  string test2 = controller.turnAngle(5);
  string test3 = controller.turnDirection(Direction);
  string test4 = controller.turnState(0);
    
  // to read the previously saved SLAM Trajectory (for path planning)
  std::ifstream file("/home/waris/ORB_SLAM2/CameraTrajectory.txt");

  cv::Mat Traj;
  int rows = 0;
  std::string line;
  while (std::getline(file, line)) {

    std::istringstream stream(line);
    while (stream) {
        string s;
        if (!std::getline(stream, s, ' ')) break;
        Traj.push_back(std::stof(s));
    }

    rows++;
  }


  Traj = Traj.reshape(1, rows);

  char State;
  char StateOrder[1000];

  float ForwardValue[1000];
  float Angle[1000];
  //int start_turning[1000];
  int stop_turning[1000];

  float disp_x_prev=0;
  float disp_z_prev=0;

  float straight;
  float turn;
  char direction;

  int k=0;
  int b=0;
  int kk=0;
  //int l=1;
  int ll=0;
  int m=0;

  int zz=0;
  int z=0;

  // going through the entire saved SLAM trajectory and breaking it down into many small straight paths. For example a right or left turn (in a curve) would broken down into many small straight lines. The number of straight lines can be increased to increase the accuracy of the path. (path planning)






  for (int i=1; i < Traj.rows; i++){

    if((sqrt(pow(Traj.at<float>(m,3) - Traj.at<float>(i,3) ,2) + pow(Traj.at<float>(m,1)  - Traj.at<float>(i,1) ,2))) > 0.02){

	  if (abs(Traj.at<float>(i,5)-Traj.at<float>(i-1,5)) > 0.004){
		ForwardValue[k] = (sqrt(pow(Traj.at<float>(m,3) - Traj.at<float>(i-1,3) ,2) + pow(Traj.at<float>(m,1)  - Traj.at<float>(i-1,1) ,2)));
		//printf("%f\n",ForwardValue[k]);
		//start_turning = i;
		Angle[k] = Traj.at<float>(i,5);

		if (Traj.at<float>(i,5) < Traj.at<float>(i-1,5)){
			State = 'l';
        }

		if (Traj.at<float>(i,5) > Traj.at<float>(i-1,5)){
			State = 'r';
        }
		StateOrder[k] = State;
		printf("%c\n",StateOrder[k]);

		if (i != Traj.rows-1){
		  k=k+1;
		}
	    //printf("%f",Traj.at<float>(i,5));

		for (int j= i+1; j < Traj.rows; j++){

			if((sqrt(pow(Traj.at<float>(i,3) - Traj.at<float>(j,3) ,2) + pow(Traj.at<float>(i,1)  - Traj.at<float>(j,1) ,2))) > 0.02){
				if (j == Traj.rows-1){
					i = j-1;
					stop_turning[z] = Traj.at<float>(j,5);
                }

				if (abs(Traj.at<float>(j,5)-Traj.at<float>(j-1,5)) <= 0.004){
					stop_turning[z] = Traj.at<float>(j-1,5);
					if (j != Traj.rows-1){
						z=z+1;
						m = j-1;
						i = j-1;
                    }
	                //	printf("   %f\n",Traj.at<float>(j-1,5));
					break;
                }
            }
         }
	
      }
    }
  }

  printf("%d, %d",k,z);


  // Housekeeping and initialization for SLAM (SLAM).
  if(argc != 4){
        cerr << endl << "Usage: ./stereo_kitti path_to_vocabulary path_to_settings path_to_sequence" << endl;
        return 1;
  }

  // calibration and rectification of cameras (SLAM)
  cv::FileStorage fsSettings(argv[2], cv::FileStorage::READ);
  if(!fsSettings.isOpened()) {
    cerr << "ERROR: Wrong path to settings" << endl;
    return -1;
  }

  cv::Mat K_l, K_r, P_l, P_r, R_l, R_r, D_l, D_r;
  fsSettings["LEFT.K"] >> K_l;
  fsSettings["RIGHT.K"] >> K_r;

  fsSettings["LEFT.P"] >> P_l;
  fsSettings["RIGHT.P"] >> P_r;

  fsSettings["LEFT.R"] >> R_l;
  fsSettings["RIGHT.R"] >> R_r;

  fsSettings["LEFT.D"] >> D_l;
  fsSettings["RIGHT.D"] >> D_r;

  int rows_l = fsSettings["LEFT.height"];
  int cols_l = fsSettings["LEFT.width"];
  int rows_r = fsSettings["RIGHT.height"];
  int cols_r = fsSettings["RIGHT.width"];

  if(K_l.empty() || K_r.empty() || P_l.empty() || P_r.empty() || R_l.empty() || R_r.empty() || D_l.empty() || D_r.empty() ||
            rows_l==0 || rows_r==0 || cols_l==0 || cols_r==0) {
    cerr << "ERROR: Calibration parameters to rectify stereo are missing!" << endl;
    return -1;
  }

  cv::Mat M1l,M2l,M1r,M2r;
  cv::initUndistortRectifyMap(K_l,D_l,R_l,P_l.rowRange(0,3).colRange(0,3),cv::Size(cols_l,rows_l),CV_32F,M1l,M2l);
  cv::initUndistortRectifyMap(K_r,D_r,R_r,P_r.rowRange(0,3).colRange(0,3),cv::Size(cols_r,rows_r),CV_32F,M1r,M2r);

  // Intializing camera feed (SLAM)
  cv::VideoCapture camera0(2);
  if (!camera0.isOpened()) {
    cerr << endl  <<"Could not open camera feed."  << endl;
    return -1;
  }

  // Create SLAM system. It initializes all system threads and gets ready to process frames. (SLAM)
  ORB_SLAM2::System SLAM(argv[1],argv[2],ORB_SLAM2::System::STEREO,true);


  cout << endl << "-------" << endl;
  cout << "Start processing sequence ..." << endl;
  

  // Main loop (contains code for SLAM and Path Planning)
  int timeStamps1=0;
  double timeStamps=0;
  cv::Mat imLeft, imRight, frame, imLeftRect, imRightRect;
  for(;;timeStamps1++) {
    camera0 >> frame;
	imLeft = frame(cv::Rect(0, 0, frame.cols/2, frame.rows));
	imRight = frame(cv::Rect(frame.cols/2, 0, frame.cols / 2, frame.rows));

	cv::remap(imLeft,imLeftRect,M1l,M2l,cv::INTER_LINEAR);
    cv::remap(imRight,imRightRect,M1r,M2r,cv::INTER_LINEAR);

	cv::Rect rect1(0, 0, 9, 240);
	cv::Rect rect2(0, 0, 320, 61);
	cv::Rect rect3(211, 0, 109, 240);
	cv::Rect rect4(0, 185, 320, 55);

	cv::Rect rect11(0, 0, 6, 240);
	cv::Rect rect22(0, 0, 320, 67);
	cv::Rect rect33(214, 0, 106, 240);
	cv::Rect rect44(0, 190, 320, 50);

	cv::rectangle(imLeftRect, rect1, cv::Scalar(0, 0, 0),CV_FILLED);
	cv::rectangle(imLeftRect, rect2, cv::Scalar(0, 0, 0),CV_FILLED);
	cv::rectangle(imLeftRect, rect3, cv::Scalar(0, 0, 0),CV_FILLED);
	cv::rectangle(imLeftRect, rect4, cv::Scalar(0, 0, 0),CV_FILLED);	

	cv::rectangle(imRightRect, rect11, cv::Scalar(0, 0, 0),CV_FILLED);
	cv::rectangle(imRightRect, rect22, cv::Scalar(0, 0, 0),CV_FILLED);
	cv::rectangle(imRightRect, rect33, cv::Scalar(0, 0, 0),CV_FILLED);
	cv::rectangle(imRightRect, rect44, cv::Scalar(0, 0, 0),CV_FILLED);	

    // Pass the images to the SLAM system (SLAM)
    SLAM.TrackStereo(imLeftRect,imRightRect,timeStamps);
	timeStamps = timeStamps + 0.033;

      if (timeStamps > 2){
        // Save new camera trajectory (SLAM). Note: this command will save the SLAM trajectory when the user will move around the lawn mower manually. This file would then be read by the Path Planning
        //code in this section to interpret it and control the motors.
        SLAM.SaveTrajectoryTUM("CameraTrajectory2.txt");

        // Real-time values of position and orientation from SLAM (these values would be used in Path Planning in a closed feedback loop to see if our robot is moving in the right direction and orientation).
        float disp_x = SLAM.x;   // detects if robot is moving in left or right in a straight direction
        float disp_y = SLAM.y;	 // detects if robot is moving up or down in a striaght direction (not used in our case)
        float disp_z = SLAM.z;   // detects if robot is moving straight or backawards in a straight direction
        float rot_x = SLAM.rx;	// roll orientation (not used yet but can be used to detect uphill or downhill)
        float rot_y = SLAM.ry;	// yaw orientation (tells us the angle at which robot turns left or right)
        float rot_z = SLAM.rz;	// pitch orientation (not used yet but can be used to detect uphill or downhill)

        // path planning code (parameters can be tweaked later).
        if (kk <= k){
          if( (ForwardValue[kk] > sqrt(pow(disp_x-disp_x_prev,2) + pow(disp_z-disp_z_prev,2)))){
		    straight =  ForwardValue[kk] - sqrt(pow(disp_x-disp_x_prev,2) + pow(disp_z-disp_z_prev,2));
		    controller.goStraightFor(straight);
		    printf ("move forward by %f", straight);
          } else{
	        controller.turnState(1);
				
	        if (StateOrder[kk] == 'r'){
		      controller.turnDirection(StateOrder[kk]);
		      controller.turnAngle(Angle[kk]);
		      printf ("turn right by %f ", Angle[kk]);
            }

	        if (StateOrder[kk] == 'l'){
		      controller.turnDirection(StateOrder[kk]);
		      controller.turnAngle(Angle[kk]);
	    	  printf ("turn left by %f ", Angle[kk]);
            }

	        if (stop_turning[zz] < rot_y){
		      printf("keep turning");
            }

	        if (stop_turning[zz] >= rot_y){
		      controller.turnState(0);
		      printf("stop turning");
		      disp_x_prev = disp_x;
		      disp_z_prev = disp_z;
		      kk = kk+1;
		      zz = zz+1;
           }
         }
       }
    }

  }

   // Stop all threads
   SLAM.Shutdown();
   return 0;
}


